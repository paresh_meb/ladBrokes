<?php   
/*
 * This API Call constantly check close time of the race using race oraganizer API. 
 * Please create cron job in linux that run this php page or run manually for testing.
 * Run Command: php updateCloseTimeAPICall.php
 */
include_once('includeObjects.php');
$data = array('request'=>'closeTime');
foreach (C::getRaceNames() as $race) {
    $data['raceName'] = $race['race_name'];
    $response = json_decode(C::callAPI(API_URL, $data), true);
    if ($response['error'] === false) {
        $responseData = $response['data'];
        if (strtolower($responseData['raceName']) === strtolower($race['race_name'])) {
            C::updateRaceCloseTime($race['race_id'], $responseData['dateTime']);
        }
    }
}