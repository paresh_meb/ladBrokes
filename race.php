<?php
// This is the race page will contain a list of all the competitors in the race, as well as their position.
include_once('includeObjects.php');
$raceId = $_REQUEST['raceId'];
if (!empty ($raceId) && is_numeric($raceId)) {
    $raceDetails = C::getRaceDetails($raceId);
    if ($raceDetails !== false) {
        $raceName = $raceDetails[0];
    } else {
        $raceName = COMPANY_NAME;
    }
    $Layout = new Layout($raceName);
    $Layout->header();
    include_once('controller/raceController.php');
} else {
    $Layout = new Layout('Error');
    $Layout->header();
    $message = 'Oops something wrong, please contact our support staff on 18005678';
    include_once('views/timeOut.php');
    error_log('race id is not valid');
}
$Layout->footer();