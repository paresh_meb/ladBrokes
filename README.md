I used following software to build this project - 

PostgreSQL 9.1 database  server,
Lighttpd 1.4.28,
PHP 5.3.10,
UBUNTU 12.04 LTS,

Installation Guide - 
please connect to postgresql and run the \i database\psql_queries.sql command or copy sql queries from the file

Please change postgres details in the config/config.php file.

API - 
You can add a cron job to run the api call file or use php updateCloseTimeAPICall.php for testing