<?php
$raceDetails = C::getRaceDetails($raceId);
$error = false;
if ($raceDetails === false) {
    $error = true;
    $message = 'Race does not exists.';
} else {
    $date = new DateTime($raceDetails[1]);
    // check if a race is closed or not
    if(new DateTime() < $date) {
        include_once('handlers/raceHandler.php');
        include_once('views/raceView.php');
    } else {
        $error = true;
        $message = "The race betting has been suspended.";
        error_log($message);
    }
}
if ($error) {
    include_once('views/timeOut.php');
}