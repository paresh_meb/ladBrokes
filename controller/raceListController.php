<?php
include_once('handlers/raceListHandler.php');
// check if are there races available to bet
if (!empty ($raceList)) {
    include_once('views/raceListView.php');
} else {
    $message = "There is no scheduled race.";
    include_once('views/timeOut.php');
}
