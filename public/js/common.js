$(document).ready(function($) {
    // This can be done through Ajax as well for smooth loading.
    $(".goToRacePage").click(function() {
        var href = $(location).attr("href");
        href += '?raceId='+$(this).data("id");
        window.document.location = href;
    });
    // Refreshing both pages every 10 seconds. It would show time out page if a race is suspended.
    setInterval(function(){location.reload();}, 10000);
});