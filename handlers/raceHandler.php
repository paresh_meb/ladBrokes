<?php
$query = "
  SELECT	
	c.competitor_name,
	p.position_no
  FROM 
	race r
  JOIN
	race_competitors rc
  ON
	rc.race_id = r.race_id
  JOIN
	competitors c
  USING
	(competitor_id)
  JOIN
	positions p
  USING
	(position_id)
  WHERE
	r.race_id = {$raceId}
  ORDER BY 
	c.competitor_name";
$competitorList = $db->getAllRows($query);
$raceDetails = C::getRaceDetails($raceId);
