<?php
$query = "
  SELECT	
	r.race_id, 
	m.meeting_name, 
	rt.race_name, 
	r.close_date_time,
	count(competitor_id) as total_competitors
  FROM 
	race r
  JOIN
	race_types rt
  USING
	(race_type_id)
  JOIN
	meetings m
  USING
	(meeting_id)
  JOIN
	race_competitors rc
  ON
	rc.race_id = r.race_id	
  WHERE
        r.close_date_time > now()
  GROUP BY
	r.race_id,
	r.close_date_time,
	m.meeting_name,
	rt.race_name
  ORDER BY 
	r.close_date_time,
        rt.race_name
  LIMIT 5";
$raceList = $db->getAllRows($query);
foreach ($raceList as &$value) {
    $date = new DateTime($value['close_date_time']);
    $value['close_date_time'] = $date->format('d/m/Y H:i:s');
}
