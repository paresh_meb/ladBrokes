<?php
header('Content-type:application/json;charset=utf-8');
function getCloseTime($raceName)
{
    // please update this date and time and call the api to change date and time for testing
    $raceCloseTime = array();
    switch (strtolower($raceName)) {
        case 'melbourne cup':
            $raceCloseTime = array("dateTime"=>'2017-03-20 14:00:00'); 
            break;
        case 'queens birthday race':
            $raceCloseTime = array("dateTime"=>'2017-03-20 14:00:00');
            break;
        case 'monthly race':
            $raceCloseTime = array("dateTime"=>'2017-03-20 14:00:00');
            break;
        case 'river race':
            $raceCloseTime = array("dateTime"=>'2017-03-20 14:00:00');
            break;
        case 'sports day':
            $raceCloseTime = array("dateTime"=>'2017-03-20 14:00:00');
            break;
        default:
            $raceCloseTime = false;
            break;
  }
  return $raceCloseTime;
}
$response = array('responseCode'=>0, 'query'=>'', 'error'=>'Required details not passed', 'data'=>'');
if (isset($_POST["request"]) && isset($_POST['raceName']))
{
    switch ($_POST["request"])
    {
        case "closeTime":
            $closeTime = getCloseTime($_POST["raceName"]);
            if (!empty($closeTime)) {
                $closeTime['raceName'] = $_POST["raceName"];
                $response['data'] = $closeTime;
                $response['error'] = false;
                $response['responseCode'] = 1;
            } else {
                $response['error'] = 'Invalid Race Name';
            }
            break;
        default:
            $response['error'] = 'Invalid Request';
            break;
    }
}

//json data
echo(json_encode($response));
