<div class="page-header">
    <h1>Race List</h1>
</div>
<table class="table table-striped">
<thead>
  <tr>
    <th>Race</th>
    <th>Location</th>
    <th>Total Competitors</th>
    <th>Closing Time</th>
  </tr>
</thead>
<tbody>
<tbody>
  <?
    foreach ($raceList as $list) { ?>
        <tr>
            <td>
                <a href="/race.php?raceId=<?= $list['race_id'] ?>">
                    <?= $list['race_name'] ?>
                </a>
            </td>
            <td>
                <?= $list['meeting_name'] ?>
            </td>
            <td>
                <?= $list['total_competitors'] ?>
            </td>
            <td>
                <?= $list['close_date_time'] ?>
            </td>
        </tr><?
    } ?>
</tbody>
</table>