<div class="page-header">
    <h1><?= $raceDetails[0] ?></h1>
</div>
<table class="table table-striped">
<thead>
  <tr>
    <th>Competitor</th>
    <th>Position</th>
  </tr>
</thead>
<tbody>
  <?
    foreach ($competitorList as $competitor) { ?>
        <tr>
            <td>
                <?= $competitor['competitor_name'] ?>
            </td>
            <td>
                <?= $competitor['position_no'] ?>
            </td>
        </tr><?
    } ?>
</tbody>
</table>
