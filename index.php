<?php
// This is the index page will always contain a list of the next 5 races that are open for betting
include_once('includeObjects.php');
$Layout = new Layout(COMPANY_NAME);
$Layout->header();
include_once('controller/raceListController.php');
$Layout->footer();
