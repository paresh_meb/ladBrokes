<?php
/*
 * This object is for interacting with database and available to all php files
 */
Class Database
{
    private $conn;
    public function __construct() {
        $this->conn = pg_connect('host=' . HOST . ' port=' . DATABASE_PORT . ' dbname=' . DATABASE_NAME . ' user=' . DATABASE_USER_NAME . ' password=' . DATABASE_PASSORD);
        if (!$this->conn) {
          error_log('An error occurred');
          exit;
        }
    }
    
    /**
     * Use this function to execute any sql query
     * @param string $query
     * @return boolean
     */
    public function query($query)
    {
        $result = pg_query($this->conn, $query);
        if (!$result) {
          error_log('An error occurred');
          exit;
        } else {
            return $result;
        }
    }
    
    /**
     * Use this function to get single record
     * @param string $query
     * @return string | Array
     */
    public function getRow($query)
    {
        $result = $this->query($query);
        if (pg_num_rows($result)>0) {
            $arr = pg_fetch_array($result, 0, PGSQL_NUM);
            if ($arr) {
                return $arr;
            } else {
                return false;
            }
        } else {
            return false;
        }
        
    }
    
    /**
     * Return all records as per a query select
     * @param string $query
     * @return array
     */
    public function getAllRows($query)
    {
        //
        $result = $this->query($query);
        $data = array();
        while ($record = pg_fetch_array($result, NULL, PGSQL_ASSOC)) {
            foreach ($record as &$value) {
                $value = htmlentities($value, ENT_QUOTES, 'UTF-8');
            }
                $data[] = $record;
        }
        return $data;
    }
}
