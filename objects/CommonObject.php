<?php
//error_reporting(E_ALL);
//ini_set('display_errors','On');
Class C
{
    public static $db;
    private static $raceName = array();
    /**
     * update close date and time
     * @param integer $race_id
     * @param dateTime $closeDateTime 
     */
    public static function updateRaceCloseTime($race_id, $closeDateTime)
    {
        $query = "
            UPDATE
                race
            SET
                close_date_time = '{$closeDateTime}'
            WHERE
                race_id = {$race_id}
            ";
        self::$db->query($query);
    }
    /**
     * Return a race details only runs once per race_id and cache the data so no need to run query again.
     * @param integer $race_id
     * @return array
     */
    public static function getRaceDetails($race_id)
    {
        if (!isset (self::$raceName[$race_id])) {
            $query = "SELECT 
                            rs.race_name,
                            r.close_date_time
                      FROM 
                            race r
                      JOIN
                            race_types rs
                      USING
                            (race_type_id)
                      WHERE
                            race_id = {$race_id};
                    ";
             self::$raceName[$race_id] = self::$db->getRow($query);
        }
        return self::$raceName[$race_id];
    }
    /**
     * get a event close date and time by calling organizer API
     * @param string $url
     * @param boolean | array $data
     * @return array
     */
    public static function callAPI($url, $data = false)
    {
        if (!empty($data)) {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data)); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($curl);
            curl_close($curl);
            return $result;
        }
    }
    /**
     * return active race 
     * @return array
     */
    public static function getRaceNames()
    {
        $query = "
              SELECT
                    r.race_id,
                    rt.race_name
              FROM 
                    race r
              JOIN
                    race_types rt
              USING
                    (race_type_id)	
              WHERE
                    r.close_date_time > now()
              ORDER BY 
                    r.close_date_time
            ";
        return self::$db->getAllRows($query);
    }
}

// initialize database object
$db = new Database();
C::$db = $db;

// set the time zone, currently it assumed it is a austrlian brisbane time zone.
date_default_timezone_set('Australia/Brisbane');
$db->query("SET TIME ZONE 'Australia/Brisbane'");
