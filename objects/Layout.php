<?php
class layout
{
    protected $title;
    function __construct($title) {
        $this->title = $title;
    }
    public function header()
    {
        $title = $this->title;
        include_once('layout/header.php');
    }
    public function footer()
    {
        include_once('layout/footer.php');
    }
}