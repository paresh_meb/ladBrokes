CREATE DATABASE ladbrokes WITH OWNER postgres;

\connect ladbrokes;

CREATE TABLE meetings (
    meeting_id serial PRIMARY KEY,
    meeting_name text not null UNIQUE
);

CREATE TABLE race_types (
    race_type_id serial PRIMARY KEY,
    race_name text NOT NULL UNIQUE
);

CREATE TABLE meeting_types (
    meeting_type_id serial PRIMARY KEY,
    type_name text NOT NULL UNIQUE
);

CREATE TABLE positions (
    position_id serial PRIMARY KEY,
    position_no integer NOT NULL UNIQUE
);

CREATE TABLE competitors (
    competitor_id serial PRIMARY KEY,
    competitor_name text NOT NULL UNIQUE
);

CREATE TABLE meetings_meeting_types (
    meeting_id integer,
    meeting_type_id integer,
    PRIMARY KEY(meeting_id, meeting_type_id),
    FOREIGN KEY(meeting_id) REFERENCES meetings(meeting_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE DEFERRABLE,
    FOREIGN KEY(meeting_type_id) REFERENCES meeting_types(meeting_type_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE DEFERRABLE
);

CREATE TABLE race (
    race_id serial PRIMARY KEY,
    meeting_id integer not null,
    race_type_id integer not null,
    close_date_time timestamp not null,
    FOREIGN KEY(meeting_id) REFERENCES meetings(meeting_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE DEFERRABLE,
    FOREIGN KEY(race_type_id) REFERENCES race_types(race_type_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE DEFERRABLE
);

CREATE TABLE race_competitors (
    race_id integer,
    position_id integer,
    competitor_id integer,
    primary key(competitor_id, position_id, race_id),
    FOREIGN KEY(race_id) REFERENCES race(race_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE DEFERRABLE,
    FOREIGN KEY(position_id) REFERENCES positions(position_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE DEFERRABLE,
    FOREIGN KEY(competitor_id) REFERENCES competitors(competitor_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE DEFERRABLE
);

INSERT INTO competitors(competitor_name)
    VALUES ('Peter hourse race pty ltd'), ('Jagna hource training center'), ('Paresh race'),
	   ('Mahesh race group'), ('Ashish enterprise');

INSERT INTO meeting_types(type_name)
    VALUES ('Thoroughbred'), ('Greyhounds'), ('Harness');

INSERT INTO meetings(meeting_name)
    VALUES ('Ramdas hourse race pty ltd'), ('Shankar ltd'), ('Sitar ltd'), ('Qld race ltd'), ('Sydne race organization');

INSERT INTO positions(position_no)
    VALUES (1), (2), (3), (4), (5), (6), (7);

INSERT INTO race_types(race_name)
    VALUES ('Melbourne Cup'), ('Queens Birthday Race'), ('Monthly Race'), ('River Race'), ('Sports day');

INSERT INTO meetings_meeting_types(
            meeting_id, meeting_type_id)
    VALUES (1, 1), (2, 2), (3, 3), (4, 1), (5, 1);

INSERT INTO race(
            meeting_id, race_type_id, close_date_time)
    VALUES (1, 1, '2017-02-28 15:10:00'), 
    (2, 5, '2017-02-28 15:00:00'), (3, 2, '2017-02-28 14:50:00'),
    (4, 3, '2017-02-28 15:20:00'), (1, 5, '2017-02-28 14:00:00'),
    (1, 3, '2017-03-01 15:20:00'), (2, 5, '2017-03-01 14:00:00');

INSERT INTO race_competitors(
            race_id, position_id, competitor_id)
    VALUES 	(1, 1, 5),(1, 2, 4),(1, 3, 3),(1, 4, 2),(1, 5, 1),
		(2, 1, 5),(2, 2, 4),(2, 3, 3),(2, 4, 2),(2, 5, 1),
		(3, 1, 5),(3, 2, 4),(3, 3, 3),(3, 4, 2),(3, 5, 1),
		(4, 1, 5),(4, 2, 4),(4, 3, 3),(4, 4, 2),(4, 5, 1),
                (6, 1, 5),(6, 2, 4),(6, 3, 3),(6, 4, 2),(6, 5, 1),
                (7, 1, 5),(7, 2, 4),(7, 3, 3),(7, 4, 2),
		(5, 1, 5),(5, 2, 4),(5, 3, 3),(5, 4, 2),(5, 5, 1);

/*delete from race_competitors;
delete from race;
update race set close_date_time='2017-02-18 14:50:00' where race_id = 1;
update race set close_date_time='2017-02-18 14:50:00' where race_id = 2;
update race set close_date_time='2017-02-18 14:50:00' where race_id = 3;
update race set close_date_time='2017-02-18 14:50:00' where race_id = 4;
update race set close_date_time='2017-02-18 14:50:00' where race_id = 5;
update race set close_date_time='2017-02-18 14:50:00' where race_id = 6;
update race set close_date_time='2017-02-18 14:50:00' where race_id = 7;
*/